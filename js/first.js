"use strict";

let userNumber = +prompt("Enter number:");
const noValidNumbers = "Sorry, no numbers";
let answer = [];
let notValid = true;

while (notValid) {
  if (Number(userNumber) === userNumber && userNumber % 1 === 0) {
    notValid = false;
  } else {
    userNumber = +prompt("Enter number:");
  }
}

for (userNumber; userNumber > 0; userNumber--) {
  if (userNumber % 5 === 0) {
    answer.push(userNumber);
  }
}

if (answer.length <= 0) {
  alert(noValidNumbers);
} else {
  alert(answer.reverse());
}
